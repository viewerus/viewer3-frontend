# Viewer 3.0
Application for visualization of sets of 3D coordinates as an interactive 3D model embedded in single page application (Angular).
Created primarily for chromosome territory visualization based on multi-layered images from confocal microscopy.

For backend code (nest.js) visit [backend repository](https://gitlab.com/viewerus/viewer3-backend).

For more information on how to use the app see the [docs](https://gitlab.com/viewerus/viewer3-backend/wikis/home) in the project's wiki page.

## Features
* Accounts
    * Login
    * Register
    * Activate (via email)
    * Reset password (via email)
* Projects
    * List view
    * Create, edit, remove
* Viewer
    * Interactive 3D visualization (pan, rotate and zoom camera)
    * Visibility control for groups
    * Locking groups
    * Making groups selectable
    * Selecting groups with different colors
    * Tree view for easier navigation
    * Exporting model to common glb format (can be used in other viewers and 3D software)
    * Download png snapshot of the model

## Environment

To run this app locally you will need git, nodejs and angular-cli.

**Install Git**
* `sudo apt update && sudo apt install git`

**Install NodeJS**
* `curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
   sudo apt-get install -y nodejs`

**Install angular-cli**
* `npm install -g @angular/cli` (may required sudo depending on system)

---

To run this app **using docker**, you will need git, docker and docker-compose.

**Install Git**
* `sudo apt update && sudo apt install git`

**Install Docker**
* `curl -fsSL https://get.docker.com -o get-docker.sh`
* `sudo sh get-docker.sh`
* `sudo usermod -aG docker $USER`
* `rm get-docker.sh`
* `sudo systemctl enable docker`

**Install Docker-Compose**
* `sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`
* `sudo chmod +x /usr/local/bin/docker-compose`

## How to run

To run locally:

1. Clone this repo: `git clone git@gitlab.com:viewerus/viewer3-frontend.git`
2. Go to project directory: `cd viewer3-frontend`
3. Install dependencies: `npm i`
4. Run development server: `npm start`
5. Navigate to `http://localhost:4200/`

One liner: `git clone git@gitlab.com:viewerus/viewer3-frontend.git && cd viewer3-frontend && npm i && npm start`

_NOTE: This assumes that you already have [node.js](https://nodejs.org/en/) and [angular-cli](https://cli.angular.io/) installed!_

## How to deploy

To deploy application you will need system with docker installed and backend part deployed (you can deploy front first).

1. Clone this repo: `git clone git@gitlab.com:viewerus/viewer3-frontend.git`
2. Go to project directory: `cd viewer3-frontend`
3. Run command `docker-compose up`

Project will be build, compressed and moved to `/var/www/viewer` directory on host machine. From there NGINX server (part of backend) will fetch it to the end-user.

## Feedback

If you found an error or have an idea for this app let us know by creating an issue.
