import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormComponent } from '@app/_shared/components/form/form.component';
import { Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { SupportService, TicketRequest, TicketType } from '@app/support/support.service';
import { ToasterService } from '@app/_core/toaster.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact.component.html'
})
export class ContactComponent extends FormComponent {
  @Input() blockEmail: boolean;
  @Output() submit = new EventEmitter<TicketRequest>();

  private _email: string;
  private _category: TicketType;
  public placeholders;

  constructor(
    private supportService: SupportService,
    private toast: ToasterService,
  ) {
    super();
    this.defaultBtnText = 'Send';
    this.loadingBtnText = 'Sending...';
    this.submitText = this.defaultBtnText;
    this.placeholders = {
      topic: {
        other: 'What is this message about?',
        bug: 'What type of bug?',
        feature: 'What feature is missing?',
      },
      message: {
        other: 'Message...',
        bug: 'The more you write the faster we can resolve this. Let us know what happened, on what page and what were you doing.',
        feature: 'Try to describe best you can what type of feature is missing, where it is needed and why you need it.',
      }
    };

  }

  buildForm() {
    this.form = this.formBuilder.group({
      email: [this.email || null, [Validators.required, Validators.email, Validators.pattern('.+@.+..+')]],
      category: [this.category || null, [Validators.required]],
      topic: [null, [Validators.required, Validators.maxLength(100)]],
      message: [null, [Validators.required]],
    });



    if (this.blockEmail) this.form.get('email').disable();
  }


  submitForm(): void {
    super.submitForm(); // Make general things first

    // Begin user authentication
    this.supportService.createTicker(this.form.value)
      .pipe(
        tap(
          data => { this.submit.emit(this.form.value); this.resetForm(); },
          error => { this.toast.error('Could not send message.', {title: 'Error!'}); }
        )
      ).subscribe();
  }

  @Input('category')
  set category(value: TicketType) {
    this._category = value;
    if (this.form) this.form.get('category').setValue(value);
  }

  get category(): TicketType {
    return this._category;
  }

  @Input('email')
  set email(value: string) {
    this._email = value;
    if (this.form) this.form.get('email').setValue(value);
  }

  get email(): string {
    return this._email;
  }

}
