import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { share, tap } from 'rxjs/operators';
import { logger } from '@app/_core/logger.service';
import { Injectable } from '@angular/core';

export interface TicketRequest {
  email: string;
  category: TicketType;
  topic: string;
  message: string;
}

export type TicketType = 'bug' | 'feature' | 'other';

@Injectable()
export class SupportService {
  constructor(
    private http: HttpClient,
  ) {}

  createTicker(ticketData: TicketRequest): Observable<any> {
    return this.http.post('/tickets', ticketData)
      .pipe(
        tap(
          data => { logger.log(`Ticket created: ${data}`); },
          error => { logger.log(`Error creating ticket: ${error}`); },
        ),
        share(),
      );
  }

  getTickets() {
    // Get all tickets for admin panel
  }

  getUserTickets() {
    //  Get all tickets for current user
  }

  getTicket() {
    // Get details about single ticket
  }

}
