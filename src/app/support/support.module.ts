import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ContactComponent } from '@app/support/contact/contact.component';
import { SupportService } from '@app/support/support.service';

@NgModule({
  declarations: [
    ContactComponent,
  ],
  providers: [
    SupportService,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
  ],
  exports: [ContactComponent],
})
export class SupportModule {}
