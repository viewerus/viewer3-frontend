import { AfterViewInit, Component, ElementRef, ViewChild, HostListener, Input } from '@angular/core';
import * as THREE from 'three';
import { Vector2 } from 'three';
import * as Stats from 'stats-js';
import { OrbitControls } from 'three-orbitcontrols-ts';
import GLTFExporter from 'three-gltf-exporter';
import {
  NzFormatEmitEvent,
  NzTreeComponent,
  NzTreeNode,
  NzTreeNodeOptions,
} from 'ng-zorro-antd';
import * as intro from 'intro.js/intro.js';

type Vec3D = [number, number, number, number?];
type Model = InputGroups[];

interface InputGroups {
  name: string;
  selectable: boolean;
  size: number;
  color: string;
  opacity: number;
  parent: string;
  children: InputGroups[] & Vec3D[];
}

type ViewerGroup = THREE.Group & {
  selectable: boolean;
  selected: boolean;
  hover: boolean;
  locked: boolean;
  originalColor: THREE.Color;
  parents: ViewerGroup[];
  children: ViewerGroup[];

  isLeaf?: boolean; // For tree view
  material?: THREE.MeshStandardMaterial;
  geometry?: THREE.SphereBufferGeometry;
};

@Component({
  selector: 'app-model-viewer',
  templateUrl: './viewer.component.html',
})
export class ViewerComponent implements AfterViewInit {
  public fixMenu: boolean;
  public showSidebar: boolean;

  /* Tree view */
  @ViewChild('treeCom') treeCom: NzTreeComponent;
  public activatedNode: NzTreeNode; // activated node
  public nodes: NzTreeNodeOptions[];

  /**/

  @Input() model: Model;
  @Input() project: any;

  private renderer: THREE.WebGLRenderer;
  private camera: THREE.PerspectiveCamera;
  public scene: THREE.Scene;

  public fieldOfView = 45;
  public nearClippingPane = 0.25;
  public farClippingPane = 2000;

  public controls;
  public groups: THREE.Group[];
  public INTERSECTED;

  private min: number;
  private max: number;

  private raycaster;
  private mouse;
  private drag: boolean;
  private stats: any;

  public colors = {
    'grey': {
      name: 'grey',
      hex: '#808080',
      color: new THREE.Color(0x808080),
    },
    'dark-blue': {
      name: 'dark-blue',
      hex: '#000075',
      color: new THREE.Color(0x000075),
    },
    'dark-yellow': {
      name: 'dark-yellow',
      hex: '#808000',
      color: new THREE.Color(0x808000),
    },
    'dark-red': {
      name: 'dark-red',
      hex: '#800000',
      color: new THREE.Color(0x800000),
    },
    'brown': {
      name: 'brown',
      hex: '#9a6324',
      color: new THREE.Color(0x9a6324),
    },
    'aquamarine': {
      name: 'aquamarine',
      hex: '#008080',
      color: new THREE.Color(0x008080),
    },
    'lime': {
      name: 'lime',
      hex: '#bcf60c',
      color: new THREE.Color(0xbcf60c),
    },
    'magenta': {
      name: 'magenta',
      hex: '#f032e6',
      color: new THREE.Color(0xf032e6),
    },
    'cyan': {
      name: 'cyan',
      hex: '#46f0f0',
      color: new THREE.Color(0x46f0f0),
    },
    'purple': {
      name: 'purple',
      hex: '#911eb4',
      color: new THREE.Color(0x911eb4),
    },
    'orange': {
      name: 'orange',
      hex: '#f58231',
      color: new THREE.Color(0xf58231),
    },
    'red': {
      name: 'red',
      hex: '#e6194b',
      color: new THREE.Color(0xe6194b),
    },
    'blue': {
      name: 'blue',
      hex: '#4363d8',
      color: new THREE.Color(0x4363d8),
    },
    'yellow': {
      name: 'yellow',
      hex: '#ffe119',
      color: new THREE.Color(0xffe119),
    },
    'green': {
      name: 'green',
      hex: '#3cb44b',
      color: new THREE.Color(0x3cb44b),
    },
  };

  public enabledColors = ['green', 'yellow', 'blue', 'orange', 'cyan', 'magenta'];
  public currentColor = this.colors['green'];
  public hoverColor = this.colors['red'];

  private white = new THREE.Color(0xffffff);
  private black = new THREE.Color(0x000000);

  public ready: boolean;

  private link: HTMLAnchorElement;
  private exporter: GLTFExporter;

  public hovered: ViewerGroup;
  public selected: ViewerGroup[];
  private mouseDown: boolean;

  @ViewChild('canvas') private canvasRef: ElementRef;
  @ViewChild('viewerCanvas') private viewerCanvas: ElementRef;

  constructor() {
    this.render = this.render.bind(this);
    this.groups = [];
    this.max = 0;
    this.min = Infinity;
    this.mouse = new Vector2();
    this.raycaster = new THREE.Raycaster();
    this.fixMenu = false;
    this.INTERSECTED = null;
    this.nodes = [];
    this.selected = [];
    this.ready = false;
    this.showSidebar = true;
    this.exporter = new GLTFExporter();
    this.link = document.createElement( 'a' );
    this.link.style.display = 'none';
    document.body.appendChild( this.link );
  }

  /* LIFECYCLE */
  ngAfterViewInit() {
    this.createScene();
    this.createCamera();
    this.createLight();
    this.startRendering();
    this.addControls();

    setTimeout(() => {
      this.ready = true;
    }, 0);

    setTimeout(() => {
      const showTutorial = JSON.parse(localStorage.getItem('show-viewer-tutorial'));
      if (showTutorial !== false) {
        this.showTutorial();
      }
    }, 100);

  }

  private createScene() {
    this.scene = new THREE.Scene();
    this.addSceneElements();
  }

  private addSceneElements() {
    this.createGroups(this.model);
    // this.addHelperAxes();
    this.addProfiler();
  }

  private createCamera() {
    const aspectRatio = this.getAspectRatio();
    this.camera = new THREE.PerspectiveCamera(
      this.fieldOfView,
      aspectRatio,
      this.nearClippingPane,
      this.farClippingPane
    );

    // Set position and look at
    this.camera.position.x = 3.3;
    this.camera.position.y = 4.5;
    this.camera.position.z = 11.5;
    this.scene.add(this.camera);
  }

  private createLight() {
    // Add basic lights
    const light1  = new THREE.AmbientLight('#ffffff', 0.25);
    light1.name = 'ambient_light';
    this.camera.add( light1 );

    const direct = new THREE.DirectionalLight( 0xffffff, 0.3 );
    this.camera.add( direct );

    const pointLight = new THREE.PointLight( 0xffffff, 1, 0 );
    this.camera.add( pointLight );

    // this.scene.add( sphereLight );
  }


  private getAspectRatio(): number {
    const height = this.canvas.clientHeight;
    if (height === 0) {
      return 0;
    }
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  private startRendering() {
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      antialias: true,
      alpha: true,
      preserveDrawingBuffer: true
    });
    this.renderer.setPixelRatio(devicePixelRatio);
    this.viewerCanvas.nativeElement.style.height = this.canvas.clientHeight + 'px';
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    this.renderer.setClearColor(0xffffff, 0);
    this.renderer.autoClear = true;
    this.renderer.toneMappingExposure = 1.5;

    const component: ViewerComponent = this;

    (function render() {
      // requestAnimationFrame(render);
      component.render();
    }());
  }

  public addControls() {
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.rotateSpeed = 1.0;
    this.controls.zoomSpeed = 1.2;
    this.controls.target.set( 0, 0, 0 ); // Anchor controls on center
    this.controls.update();
    this.controls.addEventListener('change', this.render);
  }

  public render() {
    this.stats.begin();
    this.renderer.render(this.scene, this.camera);
    this.stats.end();
  }

  /* SCENE ELEMENTS */

  createGroups(groups, parent?, parentNode?) {
    if (!groups[0].name) return;

    let parents = [];

    if (parent && parent.parents) {
      parents = parent.parents.slice();
      parents.push(parent);
    }

    for (const group of groups) {

      const newGroup = new THREE.Group();
      newGroup.name = group.name;
      newGroup['selectable'] = (group.name === 'defaults') ? false : group.selectable;
      newGroup['selected'] = false;
      newGroup['hover'] = false;
      newGroup['locked'] = false;
      newGroup['originalColor'] = new THREE.Color(group.color);

      newGroup['parents'] = parents;
      newGroup.children = [];

      const node = {
        key: newGroup.uuid,
        title: (newGroup.name === 'defaults') ? 'scene' : newGroup.name,
        group: newGroup,
        children: [],
        expanded: (newGroup.name === 'defaults'),
      };

      if (parentNode) parentNode.children.push(node);

      if (parent) {
        parent.children.push(newGroup);
      }

      const isLeaf = (group.children[0] instanceof Array);
      if (isLeaf) {

        const geometry = new THREE.SphereBufferGeometry(group.size, 32, 32);
        const material = new THREE.MeshStandardMaterial({
          color: new THREE.Color(group.color),
          metalness: 0.5,
          roughness: 0,
          flatShading: false,
        });


        if (group.opacity && group.opacity !== 1) {
          material.transparent = true;
          material.opacity = group.opacity;
        }

        newGroup['geometry'] = geometry;
        newGroup['material'] = material;
        newGroup['isLeaf'] = true;
        node['isLeaf'] = true;

        this.createPoints(newGroup, group.children);
      } else {
        this.createGroups(group.children, newGroup, node);
      }

      if (newGroup['parents'].length === 0) {
        this.nodes.push(node);
        this.scene.add(newGroup);
        this.groups.push(newGroup);
      }

    }
  }

  createPoints(group, coordinates) {

    for (const vector of coordinates) {
      const point = new THREE.Mesh( group.geometry, group.material );
      point.position.x = vector[0];
      point.position.y = vector[1];
      point.position.z = vector[2];

      if (vector[3]) {
        const scale = vector[3] / group.size;
        point.scale.x = scale;
        point.scale.y = scale;
        point.scale.z = scale;
      }

      group.add( point );
    }

  }

  addHelperAxes() {
    const axes = new THREE.AxesHelper(Math.ceil(this.max * 2));
    axes.position.x = -Math.ceil(this.max);
    axes.position.y = -Math.ceil(this.max);
    axes.position.z = -Math.ceil(this.max);
    this.scene.add( axes );
  }

  addProfiler() {
    // Add profiler
    this.stats = new Stats();
    const statsElement = this.stats.dom;
    statsElement.style.position = 'absolute';
    statsElement.style.top = '45px';
    statsElement.style.left = '60px';
    statsElement.style.display = 'none';
    statsElement.style['z-index'] = 1;
    this.canvas.parentElement.appendChild( statsElement );
  }

  /* RAYTRACING */
  highlightSelected () {
    if (this.drag) return;
    // Find intersections (objects that mouse is over)
    this.raycaster.setFromCamera( this.mouse, this.camera );
    const intersects = this.raycaster.intersectObjects( this.groups, true );


    // Proceed if there are any intersections
    if ( intersects.length > 0 ) {

      let intersected;
      for (const intersect of intersects) {
        if (intersect.object.parent.selectable) {
          intersected = intersect.object.parent;
          break;
        }
      }

      // If mouse is pointing different object
      if ( this.INTERSECTED !== intersected) {
        // If there was intersected object in last frame restore its color
        if (this.INTERSECTED && !this.INTERSECTED.selected) {
          this.toggleHover(this.INTERSECTED, false, true);
        }
        // Set INTERSECTED to currently pointed object
        this.INTERSECTED = intersected;
        if ( !this.INTERSECTED ) { return; }
        // Set color
        this.toggleHover(this.INTERSECTED);
      }

      // If there are no intersections (mouse not pointing anything)
    } else {
      // If there was intersected object in last frame restore its color
      if (!this.INTERSECTED || (this.INTERSECTED && !this.INTERSECTED.selected)) {
        this.toggleHover(this.INTERSECTED, false, true);
      }
      // Set currently pointed object to null (mouse not pointing anything)
      this.INTERSECTED = null;
    }
  }

  removeGroupHighlight(group) {
    if (!group.isLeaf) { return; }
    group.material.color = group.originalColor;
  }

  addGroupHighlight(group, render?: boolean) {
    if (!group.isLeaf) { return; }
    group.material.color = this.currentColor.color;

    if (render) this.render();
  }

  /* EVENTS */

  onMouseDown(event: MouseEvent) {
    console.log('onMouseDown');
    event.preventDefault();
    this.mouseDown = true;
  }

  onMouseUp(event: MouseEvent) {
    console.log('onMouseUp');
    this.mouseDown = false;
  }

  // Update mouse coordinates
  onMouseMove( event ) {
    event.preventDefault();
    this.drag = false;
    if (this.mouseDown) {
      this.drag = true;
    }

    this.highlightSelected();

    const boundary = this.canvas.getBoundingClientRect();
    const x = event.clientX - boundary.left;
    const y = event.clientY - boundary.top;

    this.mouse.x = ( x / this.canvas.width ) * 2 - 1;
    this.mouse.y = - ( y / this.canvas.height ) * 2 + 1;
    this.render();
  }

  // Update mouse coordinates
  onMouseClick( event ) {
    event.preventDefault();

    if (!this.INTERSECTED) { return; }
    if (this.drag) { return; }

    this.toggleSelect(this.INTERSECTED);
  }

  @HostListener('window:resize', ['$event'])
  public onResize(event: Event) {
    this.canvas.style.width = '100%';
    this.canvas.style.height = '100%';
    console.log('onResize: ' + this.canvas.clientWidth + ', ' + this.canvas.clientHeight);

    this.camera.aspect = this.getAspectRatio();
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    this.render();
  }

  @HostListener('document:keypress', ['$event'])
  public onKeyPress(event: KeyboardEvent) {
    console.log('onKeyPress: ' + event.key);
  }

  /* UTILS */

  getBackground(group: any) {
    if (!group) return;

    if (group.selected) {
      return '#' + group.material.color.getHexString();
    }

    return '#dddddd';
  }

  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  /* TREE VIEW SIDEBAR */
  openFolder(data: NzTreeNode | NzFormatEmitEvent): void {
    if (data['node'] && data['node'].isLeaf) return;

    if (data instanceof NzTreeNode) {
      data.isExpanded = !data.isExpanded;
    } else {
      data.node.isExpanded = !data.node.isExpanded;
    }
  }

  activeNode(data: NzFormatEmitEvent): void {

    if (this.activatedNode) {
      // delete selectedNodeList
      this.treeCom.nzTreeService.setSelectedNodeList(this.activatedNode);
    }
    data.node.isSelected = true;
    this.activatedNode = data.node;
    // add selectedNodeList
    this.treeCom.nzTreeService.setSelectedNodeList(this.activatedNode);
  }

  toggleSidebar() {
    this.showSidebar = !this.showSidebar;
  }

  /* SAVING AND EXPORTING */

  exportGLTF() {
    this.ready = false;

    setTimeout(() => {
      const options = {
        truncateDrawRange: false,
        binary: true,
      };

      const name = this.project.name
        .replace(/[^a-zA-Z0-9-]/gi, '_')
        .replace(/[\x00-\x1f\x80-\x9f]/g, '_')
        .toLowerCase()
        .substring(0, 80);

      this.exporter.parse( this.scene, ( result ) => {
        if ( result instanceof ArrayBuffer ) {
          this.saveArrayBuffer( result, `${name}.glb` );
        } else {
          const output = JSON.stringify( result, null, 2 );
          console.log( output );
          this.saveString( output, `${name}.gltf` );
        }
      }, options );
    }, 100);
  }

  saveString( text, filename ) {
    this.save( new Blob( [ text ], { type: 'text/plain' } ), filename );
  }

  saveArrayBuffer( buffer, filename ) {
    this.save( new Blob( [ buffer ], { type: 'application/octet-stream' } ), filename );
  }

  save( blob, filename ) {
    this.link.href = URL.createObjectURL( blob );
    this.link.download = filename;
    this.link.click();
    setTimeout(() => {
      this.ready = true;
    }, 100);
  }

  async takeScreenshot() {
    this.ready = false;

    setTimeout(() => {
      const oldSize = this.renderer.getSize();

      const width = 6000;
      const height = width / this.camera.aspect;

      this.renderer.setSize(  width, height );
      this.camera.lookAt(new THREE.Vector3(0, 0, 0));
      this.renderer.render( this.scene, this.camera, null, false );

      const name = this.project.name
        .replace(/[^a-zA-Z0-9-]/gi, '_')
        .replace(/[\x00-\x1f\x80-\x9f]/g, '_')
        .toLowerCase()
        .substring(0, 80);

      this.renderer.domElement.toBlob((blob) => {
        this.link.href = URL.createObjectURL(blob);
        this.link.download = `${name}.png`;
        this.link.click();
      }, 'image/png', 1.0);

      this.renderer.setSize(  oldSize.width, oldSize.height );
      this.camera.lookAt(new THREE.Vector3(0, 0, 0));
      this.renderer.render( this.scene, this.camera, null, false );
    }, 100);

    setTimeout(() => {
      this.ready = true;
    }, 100);
  }

  /* SETTING GROUP PROPERTIES */

  toggleVisible(group: ViewerGroup) {
    if (group.locked) return;

    if (group.visible && !group.selected) this.removeGroupHighlight(group);
    group.visible = !group.visible;
    this.setChildren(group, 'visible');
    this.updateParents(group, 'visible', 'any');

    this.render();
  }

  toggleLock(group: ViewerGroup) {
    if (!group.locked && !group.selected) this.removeGroupHighlight(group);

    group.locked = !group.locked;
    this.setChildren(group, 'locked');
    this.updateParents(group, 'locked', 'all');

    this.render();
  }

  toggleHover(group: ViewerGroup, value = true, noRender?: boolean) {
    if (this.hovered) {
      this.hovered.hover = false;
      this.hovered.material.opacity = this.hovered['originalOpacity'] || 1;
      for (const child of this.hovered.children) {
        child.scale.set(1, 1, 1);
      }
      this.hovered = undefined;
    }

    if (group && (group.locked || !group.selectable || !group.isLeaf)) return;

    if (group && value) {
      group['hover'] = value;
      this.hovered = group;
      group['originalOpacity'] = group.material.opacity || 1;
      group.material.opacity = 0.85;
      for (const child of group.children) {
        child.scale.set(1.2, 1.2, 1.2);
      }
    }

    if (!noRender) this.render();
  }

  toggleSelect(group: ViewerGroup) {
    if (group.locked || !group.selectable || !group.visible) return;

    group.selected = !group.selected;

    if (group.selected) {
      this.selected.push(group);
      this.addGroupHighlight(group);
      // group.material.opacity = group['originalOpacity'];
    } else {
      this.removeGroupHighlight(group);
      this.selected.filter(item => item.id !== group.id);
    }

    this.render();
  }

  hideAll(groups?) {
    groups = groups || this.groups[0]['children'];

    for (const group of groups) {
      if (group.isLeaf) {
        if (!group.locked) group.visible = false;
      } else {
        if (group.children) this.hideAll(group.children);
      }
    }
    this.render();
  }

  showAll(groups?) {
    groups = groups || this.groups[0]['children'];

    for (const group of groups) {
      if (group.isLeaf) {
        if (!group.locked) group.visible = true;
      } else {
        if (group.children) this.showAll(group.children);
      }
    }
    this.render();
  }

  hideSelected(groups?) {
    groups = groups || this.groups[0]['children'];

    for (const group of groups) {
      if (group.isLeaf) {
        if (!group.locked && group.selected) group.visible = false;
      } else {
        if (group.children) this.hideSelected(group.children);
      }
    }
    this.render();
  }

  showSelected(groups?) {
    groups = groups || this.groups[0]['children'];

    for (const group of groups) {
      if (group.isLeaf) {
        if (!group.locked && group.selected) group.visible = true;
      } else {
        if (group.children) this.showSelected(group.children);
      }
    }
    this.render();
  }

  clearSelection(groups?) {
    groups = groups || this.groups[0]['children'];

    for (const group of groups) {
      if (group.isLeaf) {
        if (!group.locked && group.selected) {
          group.selected = false;
          this.removeGroupHighlight(group);
        }
      } else {
        if (group.children) this.clearSelection(group.children);
      }
    }
    this.render();
  }

  toggleSelectable(group: ViewerGroup) {
    if (group.locked) return;

    if (group.selectable) {

      if (group.selected) this.toggleSelect(group);
      if (group.hover) this.toggleHover(group, false);
    }

    group.selectable = !group.selectable;
    this.setChildren(group, 'selectable');
    this.updateParents(group, 'selectable', 'any');

    this.render();
  }

  setChildren(parent: ViewerGroup, property: string) {
    if (parent.isLeaf) return;
    for (const child of parent.children) {
      if (property === 'locked' || !child.locked) child[property] = parent[property];
      this.setChildren(child, property);
    }
  }

  updateParents(group: ViewerGroup, property: string, method: 'any' | 'all') {

    // If it's a leaf, go to parent
    if (group.isLeaf) {
      const last = group.parents.length - 1;
      return this.updateParents(group.parents[last], property, method);
    }

    const allOff = group.children.every(child => !child[property]);
    const allOn = group.children.every(child => child[property]);

    if (method === 'any') {
      group[property] = !allOff;
    } else {
      group[property] = allOn;
    }

    // Repeat for parent
    if (group.parents && group.parents.length !== 0) {
      const last = group.parents.length - 1;
      this.updateParents(group.parents[last], property, method);
    }
  }

  togglePerformanceWidget() {
    const display = this.stats.dom.style.display;
    if (['', 'block'].includes(display)) {
      this.stats.dom.style.display = 'none';
    } else {
      this.stats.dom.style.display = 'block';
    }
  }

  setCurrentColor(name: string) {
    this.currentColor = this.colors[name];
  }

  showTutorial() {
    const tutorial = intro().addSteps([
      {
        element: document.querySelectorAll('#viewer')[0],
        intro: 'Viewer allows you to interact with your model.',
        scrollTo: 'tooltip'
      },
      {
        element: document.querySelectorAll('.control-panel')[0],
        intro: `In top menu you can
                </br> <b>File:</b> Save model and screens.
                </br> <b>Edit:</b> Manipulate scene.
                </br> <b>View:</b> Manage viewer panels.
                </br> <b>Help:</b> View this tutorial or hints.`,
      },
      {
        element: document.querySelectorAll('.viewer-colors')[0],
        intro: 'This panel allows you to set selection color.',
        position: 'right'
      },
      {
        element: document.querySelectorAll('.available-colors')[0],
        intro: 'This is list of available colors. Clicking one will set it as current selection color.',
        position: 'right'
      },
      {
        element: document.querySelectorAll('.current-color')[0],
        intro: 'Currently used color is display here.',
        position: 'right'
      },
      {
        element: document.querySelectorAll('.viewer-canvas')[0],
        intro: `In main panel you can interact with model with:
                </br> <b>Scroll:</b> Zoom camera.
                </br> <b>Hover:</b> Highlight object.
                </br> <b>Left click:</b> Color object.
                </br> <b>Left drag:</b> Rotate angle.
                </br> <b>Right drag:</b> Pan camera.`,
        position: 'right'
      },
      {
        element: document.querySelectorAll('.viewer-tooltip')[0],
        intro: 'Name of currently hover group will be displayed here',
        position: 'right'
      },
      {
        element: document.querySelectorAll('.viewer-tree')[0],
        intro: 'Tree panel allows you to see and manipulate objects.',
        position: 'left'
      },
      {
        element: document.querySelectorAll('.viewer-tree')[0],
        // tslint:disable-next-line:max-line-length
        intro: '<b>Open group</b> | <i class="anticon outline anticon-caret-right" nz-icon="" type="caret-right" ng-reflect-type="caret-right"><svg viewBox="0 0 1024 1024" fill="currentColor" width="1em" height="1em" data-icon="caret-right" aria-hidden="true"><path d="M715.8 493.5L335 165.1c-14.2-12.2-35-1.2-35 18.5v656.8c0 19.7 20.8 30.7 35 18.5l380.8-328.4c10.9-9.4 10.9-27.6 0-37z"></path></svg></i> <i style="background: #3f9688; color: white;" class="anticon ant-tree-switcher-icon ng-tns-c11-10 anticon-caret-down ng-star-inserted" nz-icon="" type="caret-down" ng-reflect-type="caret-down"><svg viewBox="0 0 1024 1024" fill="currentColor" width="1em" height="1em" class="ng-tns-c11-10" data-icon="caret-down" aria-hidden="true"><path d="M840.4 300H183.6c-19.7 0-30.7 20.8-18.5 35l328.4 380.8c9.4 10.9 27.5 10.9 37 0L858.9 335c12.2-14.2 1.2-35-18.5-35z"></path></svg></i> </br>' +
          'Groups can be opened and closed with toggle button or <b>double click</b>.',
        position: 'left'
      },
      {
        element: document.querySelectorAll('.viewer-tree')[0],
        // tslint:disable-next-line:max-line-length
        intro: '<b>Interactivity</b> | <i nz-icon="" type="highlight" class="anticon anticon-highlight" ng-reflect-type="highlight" ng-reflect-theme="outline" title="Make interactive"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="highlight" aria-hidden="true"><path d="M957.6 507.4L603.2 158.2a7.9 7.9 0 0 0-11.2 0L353.3 393.4a8.03 8.03 0 0 0-.1 11.3l.1.1 40 39.4-117.2 115.3a8.03 8.03 0 0 0-.1 11.3l.1.1 39.5 38.9-189.1 187H72.1c-4.4 0-8.1 3.6-8.1 8V860c0 4.4 3.6 8 8 8h344.9c2.1 0 4.1-.8 5.6-2.3l76.1-75.6 40.4 39.8a7.9 7.9 0 0 0 11.2 0l117.1-115.6 40.1 39.5a7.9 7.9 0 0 0 11.2 0l238.7-235.2c3.4-3 3.4-8 .3-11.2zM389.8 796.2H229.6l134.4-133 80.1 78.9-54.3 54.1zm154.8-62.1L373.2 565.2l68.6-67.6 171.4 168.9-68.6 67.6zM713.1 658L450.3 399.1 597.6 254l262.8 259-147.3 145z"></path></svg></i> <i nz-icon="" type="highlight" class="anticon anticon-highlight" ng-reflect-type="highlight" ng-reflect-theme="fill" title="Ignore object"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="highlight" aria-hidden="true"><path d="M957.6 507.4L603.2 158.2a7.9 7.9 0 0 0-11.2 0L353.3 393.4a8.03 8.03 0 0 0-.1 11.3l.1.1 40 39.4-117.2 115.3a8.03 8.03 0 0 0-.1 11.3l.1.1 39.5 38.9-189.1 187H72.1c-4.4 0-8.1 3.6-8.1 8V860c0 4.4 3.6 8 8 8h344.9c2.1 0 4.1-.8 5.6-2.3l76.1-75.6 40.4 39.8a7.9 7.9 0 0 0 11.2 0l117.1-115.6 40.1 39.5a7.9 7.9 0 0 0 11.2 0l238.7-235.2c3.4-3 3.4-8 .3-11.2z"></path></svg></i> </br>' +
          'To interact (hover, select) with object this icon must be filled. ' +
          'Changing property for group will change it for all its children.',
        position: 'left'
      },
      {
        element: document.querySelectorAll('.viewer-tree')[0],
        // tslint:disable-next-line:max-line-length
        intro: '<b>Visibility</b> | <i nz-icon="" type="eye" class="anticon anticon-eye" ng-reflect-type="eye" ng-reflect-theme="fill" title="Hide"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="eye" aria-hidden="true"><path d="M396 512a112 112 0 1 0 224 0 112 112 0 1 0-224 0zm546.2-25.8C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 0 0 0 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM508 688c-97.2 0-176-78.8-176-176s78.8-176 176-176 176 78.8 176 176-78.8 176-176 176z"></path></svg></i> <i nz-icon="" type="eye" class="anticon anticon-eye" ng-reflect-type="eye" ng-reflect-theme="outline" title="Show"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="eye" aria-hidden="true"><path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 0 0 0 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path></svg></i> </br>' +
          'Determines if object will be visible in the main panel.',
        position: 'left'
      },
      {
        element: document.querySelectorAll('.viewer-tree')[0],
        // tslint:disable-next-line:max-line-length
        intro: '<b>Lock</b> | <i nz-icon="" class="anticon anticon-unlock" ng-reflect-type="unlock" ng-reflect-theme="outline" title="Lock"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="unlock" aria-hidden="true"><path d="M832 464H332V240c0-30.9 25.1-56 56-56h248c30.9 0 56 25.1 56 56v68c0 4.4 3.6 8 8 8h56c4.4 0 8-3.6 8-8v-68c0-70.7-57.3-128-128-128H388c-70.7 0-128 57.3-128 128v224h-68c-17.7 0-32 14.3-32 32v384c0 17.7 14.3 32 32 32h640c17.7 0 32-14.3 32-32V496c0-17.7-14.3-32-32-32zm-40 376H232V536h560v304zM484 701v53c0 4.4 3.6 8 8 8h40c4.4 0 8-3.6 8-8v-53a48.01 48.01 0 1 0-56 0z"></path></svg></i> <i nz-icon="" class="anticon anticon-lock" ng-reflect-type="lock" ng-reflect-theme="fill" title="Unlock"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="lock" aria-hidden="true"><path d="M832 464h-68V240c0-70.7-57.3-128-128-128H388c-70.7 0-128 57.3-128 128v224h-68c-17.7 0-32 14.3-32 32v384c0 17.7 14.3 32 32 32h640c17.7 0 32-14.3 32-32V496c0-17.7-14.3-32-32-32zM540 701v53c0 4.4-3.6 8-8 8h-40c-4.4 0-8-3.6-8-8v-53a48.01 48.01 0 1 1 56 0zm152-237H332V240c0-30.9 25.1-56 56-56h248c30.9 0 56 25.1 56 56v224z"></path></svg></i> </br>' +
          'If object is locked all interactions with it all ignored until unlocked.',
        position: 'left'
      },
      {
        element: document.querySelectorAll('.viewer-tree')[0],
        // tslint:disable-next-line:max-line-length
        intro: '<b>Selection</b> | <i nz-icon="" class="anticon anticon-stop ng-star-inserted" ng-reflect-type="stop" ng-reflect-theme="outline" title="Not interactive"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="stop" aria-hidden="true"><path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372 0-89 31.3-170.8 83.5-234.8l523.3 523.3C682.8 852.7 601 884 512 884zm288.5-137.2L277.2 223.5C341.2 171.3 423 140 512 140c205.4 0 372 166.6 372 372 0 89-31.3 170.8-83.5 234.8z"></path></svg></i> <i nz-icon="" class="anticon anticon-bulb ng-star-inserted" ng-reflect-type="bulb" ng-reflect-theme="outline" title="Select"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="bulb" aria-hidden="true"><path d="M632 888H392c-4.4 0-8 3.6-8 8v32c0 17.7 14.3 32 32 32h192c17.7 0 32-14.3 32-32v-32c0-4.4-3.6-8-8-8zM512 64c-181.1 0-328 146.9-328 328 0 121.4 66 227.4 164 284.1V792c0 17.7 14.3 32 32 32h264c17.7 0 32-14.3 32-32V676.1c98-56.7 164-162.7 164-284.1 0-181.1-146.9-328-328-328zm127.9 549.8L604 634.6V752H420V634.6l-35.9-20.8C305.4 568.3 256 484.5 256 392c0-141.4 114.6-256 256-256s256 114.6 256 256c0 92.5-49.4 176.3-128.1 221.8z"></path></svg></i> <i nz-icon="" style="color: rgb(60, 180, 75);" class="anticon anticon-bulb ng-star-inserted" ng-reflect-type="bulb" ng-reflect-theme="fill" title="Selected"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="bulb" aria-hidden="true"><path d="M348 676.1C250 619.4 184 513.4 184 392c0-181.1 146.9-328 328-328s328 146.9 328 328c0 121.4-66 227.4-164 284.1V792c0 17.7-14.3 32-32 32H380c-17.7 0-32-14.3-32-32V676.1zM392 888h240c4.4 0 8 3.6 8 8v32c0 17.7-14.3 32-32 32H416c-17.7 0-32-14.3-32-32v-32c0-4.4 3.6-8 8-8z"></path></svg></i> <i nz-icon="" style="color: rgb(67, 99, 216);" class="anticon anticon-bulb ng-star-inserted" ng-reflect-type="bulb" ng-reflect-theme="fill" title="Selected"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="bulb" aria-hidden="true"><path d="M348 676.1C250 619.4 184 513.4 184 392c0-181.1 146.9-328 328-328s328 146.9 328 328c0 121.4-66 227.4-164 284.1V792c0 17.7-14.3 32-32 32H380c-17.7 0-32-14.3-32-32V676.1zM392 888h240c4.4 0 8 3.6 8 8v32c0 17.7-14.3 32-32 32H416c-17.7 0-32-14.3-32-32v-32c0-4.4 3.6-8 8-8z"></path></svg></i> </br>' +
          'If object is interactive, the icon will be buld. Clicking this icon will color the object with currently set color.',
        position: 'left'
      },
      {
        element: document.querySelectorAll('.viewer-canvas')[0],
        intro: 'That\'s pretty much it. </br> Good luck!',
        position: 'right',
        scrollTo: 'tooltip'
      },
    ]);

    tutorial.setOptions({
      exitOnOverlayClick: false,
      showStepNumbers: false,
      showProgress: true,
    });

    tutorial.start()
      .onexit(() => { localStorage.setItem('show-viewer-tutorial', 'false'); });

  }

}

