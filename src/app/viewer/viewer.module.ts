import { NgModule } from '@angular/core';
import { ViewerComponent } from '@app/viewer/viewer.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    NgZorroAntdModule,
  ],
  declarations: [ViewerComponent],
  exports: [ViewerComponent],
})
export class ViewerModule {}
