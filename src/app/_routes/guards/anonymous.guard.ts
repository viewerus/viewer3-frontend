import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '@app/authentication/authentication.service';
import { ToasterService } from '@app/_core/toaster.service';
import { logger } from '@app/_core/logger.service';

@Injectable({
  providedIn: 'root'
})
export class AnonymousGuard implements CanActivate {
  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private toast: ToasterService,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkPermission();
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkPermission();
  }

  checkPermission(): boolean {
    // Redirect to home page
    if (this.auth.isLoggedIn) {
      logger.log('Logged in, redirecting to home page...');
      this.router.navigate(['/']).then();
    }
    // True is NOT logged in
    return !this.auth.isLoggedIn;
  }

}
