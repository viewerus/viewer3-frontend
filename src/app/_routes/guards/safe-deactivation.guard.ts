import { CanDeactivate } from '@angular/router';
import { ProjectCreateComponent } from '@app/projects/project-create/project-create.component';
import { Injectable } from '@angular/core';

@Injectable()
export class SafeDeactivationGuard implements CanDeactivate<ProjectCreateComponent> {

  canDeactivate(component: ProjectCreateComponent): boolean {

    if (component.hasUnsavedData()) {
      return confirm('You have unsaved changes! If you leave, your changes will be lost.');
    }
    return true;
  }

}
