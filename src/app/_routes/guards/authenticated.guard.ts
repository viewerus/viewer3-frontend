import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../../authentication/authentication.service';
import { logger } from '@app/_core/logger.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedGuard implements CanActivate {
  constructor(
    private auth: AuthenticationService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkPermission(state);
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkPermission(state);
  }

  checkPermission(state): Promise<boolean> | boolean {
    // Redirect to login page
    if (!this.auth.isLoggedIn) {
      logger.log('NOT logged in, redirecting to login page...');
      this.router.navigate(['/login'], {
        queryParams: { returnUrl: state.url },
      });
    }

    return this.auth.isLoggedIn;
  }

}
