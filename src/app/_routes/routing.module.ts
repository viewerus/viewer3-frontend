import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './routes';
import { SafeDeactivationGuard } from '@app/_routes/guards/safe-deactivation.guard';


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [SafeDeactivationGuard],
  exports: [RouterModule]
})
export class RoutingModule { }
