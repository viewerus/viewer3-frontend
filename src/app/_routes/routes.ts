import { Routes } from '@angular/router';
/* COMPONENTS */
import { LayoutComponent } from '../layout/layout/layout.component';
import { AuthenticatedGuard } from './guards/authenticated.guard';
import { AnonymousGuard } from './guards/anonymous.guard';
import { BoxComponent } from '../layout/box/box.component';
import { ActivationComponent } from '../authentication/activation/activation.component';
import { LoginComponent } from '../authentication/login/login.component';
import { RegistrationComponent } from '../authentication/registration/registration.component';
import { AccountRecoveryComponent } from '../authentication/account-recovery/account-recovery.component';
import { PasswordResetComponent } from '../authentication/password-reset/password-reset.component';
import { ErrorPageComponent } from '../_shared/components/error-page/error-page.component';
import { ProjectListComponent } from '@app/projects/project-list/project-list.component';
import { ProjectCreateComponent } from '@app/projects/project-create/project-create.component';
import { ProjectDetailsComponent } from '@app/projects/project-details/project-details.component';
import { ProjectListResolver } from '@app/projects/_resolvers/project-list.resolver';
import { ProjectDetailsResolver } from '@app/projects/_resolvers/project-details.resolver';
import { SafeDeactivationGuard } from '@app/_routes/guards/safe-deactivation.guard';

export const routes: Routes = [
  /* MAIN LAYOUT */
  {
    path: '',
    component: LayoutComponent,
    canActivateChild: [AuthenticatedGuard],
    children: [
      {path: '', redirectTo: 'projects', pathMatch: 'full'},

      /* MODELS */
      {
        path: 'projects',
        component: ProjectListComponent,
        resolve: { resolverData: ProjectListResolver },
      },
      {
        path: 'projects/new',
        component: ProjectCreateComponent,
        canDeactivate: [SafeDeactivationGuard]
      },
      {
        path: 'projects/:id',
        component: ProjectDetailsComponent,
        resolve: { resolverData: ProjectDetailsResolver },
      },
    ],
  },

  /* BOX LAYOUT */
  {
    path: '',
    component: BoxComponent,
    canActivateChild: [AnonymousGuard],
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegistrationComponent },
      { path: 'activate/:token', component: ActivationComponent },
      { path: 'recover-account', component: AccountRecoveryComponent },
      { path: 'reset-password/:token', component: PasswordResetComponent },
    ],
  },

  { path: 'error/:code', component: ErrorPageComponent },
  { path: '**', component: ErrorPageComponent },
];
