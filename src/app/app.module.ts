import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RoutingModule } from './_routes/routing.module';
import { CoreModule } from './_core/core.module';
import { SharedModule } from './_shared/shared.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { LayoutModule } from './layout/layout.module';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { InjectorService } from '@app/_core/injector.service';
import { LoggerService } from '@app/_core/logger.service';
import { HttpInterceptorService } from '@app/_core/http-interceptor.service';
import { ProjectModule } from '@app/projects/project.module';
import { ElasticInputModule } from 'angular2-elastic-input';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    CoreModule,
    SharedModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ElasticInputModule.forRoot(),
    NgZorroAntdModule,
    /* FEATURES */
    ProjectModule,
    AuthenticationModule,
    LayoutModule,
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  // Initialize some classes
  constructor(
    private injectorInit: InjectorService,
    private loggerInit: LoggerService,
  ) {}

}
