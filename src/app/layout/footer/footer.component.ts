import { Component } from '@angular/core';
import { AuthenticationService } from '@app/authentication/authentication.service';
import { TicketType } from '@app/support/support.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent {
  public visible: boolean;
  public category: TicketType;
  public user;
  public success;

  constructor(
    private auth: AuthenticationService,
  ) {
    this.visible = false;
    this.user = this.auth.user;
  }

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
    setTimeout(() => {
      this.success = false;
    }, 200);
  }

  showSuccessBox() {
    this.success = true;
  }

}
