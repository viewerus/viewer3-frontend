import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { BoxComponent } from './box/box.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { SupportModule } from '@app/support/support.module';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    BoxComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgZorroAntdModule,
    SupportModule,
  ]
})
export class LayoutModule { }
