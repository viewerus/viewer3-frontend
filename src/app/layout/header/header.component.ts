import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/authentication/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  public email: string;
  public admin: boolean;

  constructor(
    private auth: AuthenticationService
  ) {
    this.init();
  }

  init() {
    this.email = '';

    const user = this.auth.user;
    this.email = user && user.email;
    this.admin = user && user.admin;
  }

  logout() {
    this.auth.logout();
  }

}
