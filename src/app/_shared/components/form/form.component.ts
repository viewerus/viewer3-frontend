import { Component, HostListener, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { injector } from '@app/_core/injector.service';

/**
 * Component used as base for all components displaying a form.
 * Contains method for building, validating and form manipulation.
 */
@Component({
  selector: 'app-form',
  template: ``,
})
export class FormComponent implements OnInit {
  public form: FormGroup;
  public formBuilder: FormBuilder;
  public loading: boolean; // Display spinner if true
  public submitted: boolean; // Used to display information after submission
  public errors: any; // Adjust after-submit page accordingly

  public defaultBtnText: string; // Text on submit button by default
  public loadingBtnText: string; // Text on submit button while loading
  public submitText: string; // Text currently displayed on submit button

  // Save removed controls in this object
  public savedControls: {[s: string]: FormControl};

  constructor() {
    this.formBuilder = injector.get(FormBuilder);
    this.submitText = 'Submit';
    this.savedControls = {};
  }

  /**
   * Get hint about the error for user.
   * Either from the error or from predefined list.
   */
  static getErrorHint(errors): string {
    let hint = '';

    const errorName = Object.keys(errors)[0];
    const error = errors[errorName];

    switch (errorName) {
      case 'required': hint = 'This field is required'; break;
      case 'min': hint = `Minimal value: ${error.min}.`; break;
      case 'max': hint = `Maximum value: ${error.max}.`; break;
      case 'requiredTrue': hint = 'The field must be selected.'; break;
      case 'email': hint = 'Please enter valid email'; break;
      case 'minlength': hint = `Too short. Minimum length: ${error.requiredLength} characters`; break;
      case 'maxLength': hint = `Too long. Maximum length: ${error.requiredLength} characters`; break;
      default: hint = (typeof error === 'string') ? error : 'Invalid field value.';
    }

    return hint;
  }

  ngOnInit() {
    this.buildForm();
  }

  /**
   * Method used to build a form model.
   * Overwrite it in your child class and create form there.
   */
  buildForm() {
    // TODO make this more generic
    // TODO allow building forms from models
  }

  /**
   * Method called when form is submitted.
   * Most data manipulation before sending request should be done here.
   * Overwrite this method in child class to process form submit event.
   */
  submitForm(): void {
    // Close form
    this.form.disable();
    this.loading = true;
    this.submitText = this.loadingBtnText;
  }

  /**
   * Restore initial form state. If force is true inputs are cleared.
   * @param force if true clear input values
   */
  resetForm(force: boolean = false): void {
    // Clear inputs when needed
    if (force) {
      this.form.reset();
    }

    // Restore initial state
    this.submitText = this.defaultBtnText;
    this.loading = false;
    this.form.enable();
  }

  triggerFieldUpdate(field): void {
    Promise.resolve().then(() => this.form.controls[field].updateValueAndValidity());
  }

  triggerFormUpdate(): void {
    for (const i in this.form.controls) {
      if (this.form.controls.hasOwnProperty(i)) {
        this.form.controls[i].markAsDirty();
        this.form.controls[i].updateValueAndValidity();
      }
    }
  }

  /**
   * Wrapper for accessing error hint from templates
   */
  getErrorHint(fieldName: string) {
    return FormComponent.getErrorHint(this.form.get(fieldName).errors);
  }

  /**
   * Dynamically add field to form.
   */
  addField(controlName: string) {
    this.form.addControl(controlName, this.savedControls[controlName]);
    delete this.savedControls[controlName];
  }

  /**
   * Dynamically remove field from form.
   */
  removeField(controlName: string) {
    this.savedControls[controlName] = <FormControl>this.form.controls[controlName];
    this.form.removeControl(controlName);
  }

  fieldInvalid(fieldName: string) {
    return this.form.get(fieldName).dirty
           && this.form.get(fieldName).errors;
  }

  isRequired(fieldName: string): boolean {
    const fieldValidator = this.form.controls[fieldName].validator;
    const validator = fieldValidator && fieldValidator({} as AbstractControl);
    return validator && validator['required'];
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.hasUnsavedData()) {
      $event.returnValue = true;
    }
  }

  hasUnsavedData(): boolean {
    return this.form.dirty;
  }

}
