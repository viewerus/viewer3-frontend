import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { logger } from '@app/_core/logger.service';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
})
export class ErrorPageComponent implements OnInit {

  public page: any;
  private errors: any = {
    404: {
      title: '404',
      subtitle: 'Page not found!',
      description: 'I have no idea how you got here ' + 'and at this point I\'m too afraid to ask.',
      showPath: true,
      showButton: true,
      path: location.pathname,
    },
    unknown: {
      title: 'Strange...',
      subtitle: 'Unknown error just occurred!',
      description:
        'We don\'t really know what happened yet but you can ' + 'try to use the button we prepared for you below.',
      showPath: false,
      showButton: true,
      path: location.pathname,
    },
  };

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    // Get error code from route
    const code = this.route.snapshot.paramMap.get('code') || '404';
    logger.trace(`Loading error page for error ${code}`);

    // Display error info on page
    this.page = this.errors[code] || this.errors['unknown'];
  }

}
