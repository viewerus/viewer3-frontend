import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { FormComponent } from '@app/_shared/components/form/form.component';
import { logger } from '@app/_core/logger.service';

interface Filters {
  [key: string]: string[];
}

/**
 * Component for basic table properties and method.
 * Other component that use tables should extend it.
 */
@Component({
  selector: 'app-table',
  template: ``,
})
export class TableComponent implements OnInit {
  public ready: boolean;
  /* PAGE PARAMS */
  public pageData: any; // Data for current page
  public page: number; // Current page index
  public limit: number; // Number of items per page
  public itemsCount: number; // Total number of items
  public filters: Filters; // Object with filters as properties
  public sortColumn; // Column to be sorted
  public sortReverse; // Descending order if true

  /* TABLE */
  public allChecked: boolean; // All items on page checked
  public someChecked: boolean; // Some items on page checked
  public checkedCount: number; // How many items are checked
  public loading: boolean; // Whether data is loading
  public sortMap: any; // Mapping for dynamic sorting
  public searchMap: any;

  /* FETCHED DATA */
  public data: any; // All available data

  constructor() {
    this.ready = false;
  }

  ngOnInit() {
    this.initProperties();
  }

  /**
   * Assign default or initial values to properties.
   */
  initProperties() {
    this.allChecked = false;
    this.someChecked = false;
    this.checkedCount = 0;
    this.itemsCount = 0;
    this.loading = true;
    this.filters = {};
    this.page = 1;
    this.limit = 10;
    this.sortColumn = '';
    this.sortMap = {};
    this.searchMap = {};
    this.data = [];
    this.pageData = [];
  }

  /**
   * Sets sort value after user action.
   */
  sort(columnName: string, value?: 'ascend' | 'descend') {
    logger.trace('Setting sorting for', columnName);
    this.sortMap[columnName] = value;

    if (!value && this.sortMap[columnName]) {
      this.sortMap[columnName] = this.sortMap[columnName] = 'ascend'
          ? 'descend'
          : 'ascend';
    }

    this.sortMap[columnName] = this.sortMap[columnName] || 'ascend';

    this.sortColumn = columnName;
    this.sortReverse = this.sortMap[columnName] === 'descend';

    // Set sorting flag for UI
    for (const key in this.sortMap) {
      if (!this.sortMap.hasOwnProperty(key)) continue;
      this.sortMap[key] = (key === columnName ? this.sortMap[key] : null);
    }

    this.fetchPage();
  }

  /**
   * Sets filters according to user choices.
   */
  // TODO filters widget should be done with flex-box columns (open ant issue)
  filter(filters: string[], propertyName: string) {
    logger.trace('Setting filter', propertyName, filters);
    this.filters[propertyName] = filters;
    this.page = 1;

    this.fetchPage();
  }

  /**
   * Get data for current page with set params.
   */
  async fetchPage() {
    this.loading = true;

    // Start with all data
    let fetchedData = this.data;

    logger.trace('Fetching page with data', fetchedData);
    if (fetchedData.length !== 0) {
      // Apply filters
      for (const key in this.filters) {
        if (!this.filters.hasOwnProperty(key)) continue;
        if (!this.filters[key] || this.filters[key].length === 0) continue;

        fetchedData = fetchedData
          .filter((item) => {
            return this.filters[key].some((filter) => {
              return filter === _.get(item, key);
            });
          });

      }

      // Filter searching
      for (const field of Object.keys(this.searchMap)) {
        fetchedData = fetchedData.filter((item) => {
          return _.get(item, field).includes(this.searchMap[field]);
        });
      }

      logger.trace('After filtering', fetchedData);

      // Correct sortColumn
      this.sortColumn = (this.sortColumn === 'status') ? 'status.description' : this.sortColumn;

      // Sort results
      fetchedData = fetchedData
        .sort((a, b) => {
          const x = _.get(a, this.sortColumn);
          const y = _.get(b, this.sortColumn);
          return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });

      // Reverse order if necessary
      if (this.sortReverse) {
        fetchedData.reverse();
      }
      logger.trace('After sorting', fetchedData);

      // Get current page
      const pageStart = this.limit * (this.page - 1);
      const pageEnd = pageStart + this.limit;
      fetchedData = fetchedData.slice(pageStart, pageEnd);
      logger.trace('After pagination', fetchedData);
    }

    // Update table data
    this.pageData = fetchedData;
    this.updateChecked();
    this.loading = false;
  }

  /**
   * Mark all non-disabled items as checked
   */
  checkAll(value: boolean): void {
    this.pageData.forEach(data => {
      if (!data.disabled) {
        data.checked = value;
      }
    });
    this.updateChecked();
  }

  /**
   * Update information about checked elements.
   */
  updateChecked(): void {
    const allChecked = this.pageData
      .filter(value => !value.disabled).every(value => value.checked === true);
    const allUnChecked = this.pageData
      .filter(value => !value.disabled).every(value => !value.checked);

    this.allChecked = allChecked;
    this.someChecked = (!allChecked) && (!allUnChecked);
    this.checkedCount = this.pageData.filter(value => value.checked).length;
    this.afterCheckedUpdate();
  }

  /**
   * Used by child to implement logic related to check change.
   */
  afterCheckedUpdate() {}

  /**
   * Get all checked rows
   * @param propertyName Returns only given property instead of the whole object.
   */
  checked(propertyName?: string) {
    const checked = this.pageData.filter(value => value.checked === true);

    if (propertyName) {
      return checked.map(item => item[propertyName]);
    }

    return checked || [];
  }

  getErrorHint(error) {
    return FormComponent.getErrorHint(error);
  }

}
