import { FormControl, ValidatorFn } from '@angular/forms';

/**
 * Class for custom form validators.
 * Each validator takes FormControl and must return boolean value.
 * Add description and use example to each validator in jsDOC comment.
 */
export class CustomValidators {
  /**
   * Checks if value of fields 'password' and 'confirmPassword' match.
   * Add to 'confirmPassword' field. (you can change that name)
   * Form MUST field named 'password'. (you CANNOT change that name)
   */
  public static passwordsMatch(control: FormControl): { [s: string]: string | boolean } {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== control.parent.controls['password'].value) {
      return { confirm: 'Passwords don\'t match' };
    }
  }

  public static positiveInteger(control: FormControl): { [s: string]: string | boolean } {
    const RE_POS_INT = RegExp('^[1-9][0-9]*$', 'g');
    const value = control.value + ''; // Convert to string

    // Empty value is valid if field is not required
    if (value === null || value === '') {
      return null;
    }

    if (value.includes('.') || value.includes(',') || !RE_POS_INT.test(value)) {
      return { notInteger: 'Value is not a positive integer.' };
    }
  }

  /*  WITH PARAMS */

  public static isEnum(enumClass: any): ValidatorFn {
    return (control: FormControl): { [s: string]: string | boolean } | null => {

      // Empty value is valid if field is not required
      if (control.value === null || control.value === '') {
        return null;
      }

      if (Object.values(enumClass).includes(control.value)) {
        return null;
      }
      return { invalidType: 'Value invalid for this field.' };
    };
  }
}
