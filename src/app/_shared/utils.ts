import { Observable } from 'rxjs';

/**
 * This class should only contains static methods.
 * Generic ones that can be used anywhere.
 */
export class Utils {
  /**
   * Check if we are on localhost (by hostname)
   */
  public static get isLocalDomain(): boolean {
    const locals = ['0.0.0.0', 'localhost', '127.0.0.1'];
    return locals.indexOf(location.hostname) >= 0;
  }

  /**
   * Simple method for checking whether local storage is available.
   * This method tries to add item to ls and if it succeeds returns true.
   */
  public static get isLocalStorageAvailable(): boolean {
    const test = 'testingLocalStorage';
    try {
      localStorage.setItem(test, test);
      localStorage.removeItem(test);
      return true;
    } catch (e) {
      // Note: this will also fail if we exceed local storage limit!
      return false;
    }
  }

  public static decodeJwt(token: string) {
    const payloadString = token.split('.')[1];
    try {
      const payload = JSON.parse(atob(payloadString));
      return payload;
    } catch (e) {
      return null;
    }

  }

  public static readFile(image: Blob): Observable<string | ArrayBuffer> {

    return Observable.create(observer => {
      const reader = new FileReader();

      reader.addEventListener('load', () => {
        observer.next(reader.result);
        observer.complete();
      }, false);

      if (image) reader.readAsDataURL(image);
    });

  }

}
