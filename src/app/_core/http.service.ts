import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/**
 * Service for basic HTTP requests.
 * Currently its only purpose is to have centralized httpClient.
 * This way we don't have to import HttpClient in every module with request.
 */
@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  /**
   * Performs a request with `get` http method.
   */
  get(url: string, options?: any): Observable<any> {
    return this.http.get(url, options);
  }

  /**
   * Performs a request with `post` http method.
   */
  post(url: string, body: any, options?: any): Observable<any> {
    return this.http.post(url, body, options);
  }

  /**
   * Performs a request with `put` http method.
   */
  put(url: string, body: any, options?: any): Observable<any> {
    return this.http.put(url, body, options);
  }

  /**
   * Performs a request with `delete` http method.
   */
  delete(url: string, options?: any): Observable<any> {
    return this.http.delete(url, options);
  }

  /**
   * Performs a request with `patch` http method.
   */
  patch(url: string, body: any, options?: any): Observable<any> {
    return this.http.patch(url, body, options);
  }

  /**
   * Performs a request with `head` http method.
   */
  head(url: string, options?: any): Observable<any> {
    return this.http.head(url, options);
  }

}
