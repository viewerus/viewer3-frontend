import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { logger } from '@app/_core/logger.service';
import { environment } from '@env/environment';
import { tap } from 'rxjs/operators';
import { ToasterService } from '@app/_core/toaster.service';
import { StorageService } from '@app/_core/storage.service';
import { AuthenticationService } from '@app/authentication/authentication.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
    private toast: ToasterService,
    private store: StorageService,
    private router: Router,
    private auth: AuthenticationService,
  ) { }

  /**
   * Change request before sending it to the backend.
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    logger.trace('Intercepted request', request);

    // Change destination to server
    request = this.redirectRequests(request);
    logger.trace('Changed destination', request.url);

    // Add Authorization header to requests
    request = this.addAuthorizationHeader(request);
    logger.trace('Added authorization token', request.headers);

    // Handle errors globally
    logger.info('Sending request...', request);
    return this.interceptHttpErrors(request, next);
  }

  /**
   * Add serve protocol and host from environment if path is incomplete.
   */
  private redirectRequests(request: HttpRequest<any>) {
    const prefix = `/api/v${environment.apiVersion}`;
    return request.clone({
      url: environment.BACKEND_URL + prefix + request.url
    });
  }

  /**
   * Append authorization header with bearer token to the request.
   */
  private addAuthorizationHeader(request: HttpRequest<any>): HttpRequest<any> {

    // Get token from user object
    const user = this.store.getItem('user');
    const token = user && JSON.parse(user).access_token;

    // Don't proceed without token
    if (!token) return request;

    const authHeader = token && `Bearer ${token}`;

    // Append token to headers
    return request.clone({
      setHeaders: { authorization: authHeader },
    });
  }

  /**
   * Every request goes through here.
   */
  private interceptHttpErrors(request: HttpRequest<any>, next: HttpHandler) {
    return next
      .handle(request)
      .pipe(
        tap(
          data => {
            // Request successful
            logger.log('Request successful', request.url, data);
          },
          error => {
            // Error happened
            logger.error('Error in request', request.url, error.status, error.message);

            // Get error type (0, 100, 200, 300, 400, 500)
            const errorCategory = Math.floor(error.status / 100) * 100;

            // Server errors | 5xx
            if (errorCategory === 500 || errorCategory === 0) {
              this.showServerErrorToast();
            }

            // Don't show generic messages for auth errors
            if (request.url.includes('/auth/')) return;

            if (error.status === 403) {
              this.router.navigate(['/']);
              this.showAuthorizationError();
            }

            if (error.status === 401) {
              this.auth.logout();
              this.showAuthenticationError();
            }

            if (error.status === 400) {
              this.showPayloadError();
            }

          }
        )
      );
  }


  /**
   * Simple method for displaying generic server error toast
   */
  private showServerErrorToast() {
    this.toast.error(
      `Something went wrong on our side.` +
      `Try again later and if the problem persists contact us.`,
      { title: 'Server error!' }
    );
  }

  /**
   * Simple method for displaying generic server error toast
   */
  private showAuthenticationError() {
    this.toast.warn(
      `Your session has expired. Log in again.`,
      { title: 'Session expired!', duration: 0 }
    );
  }

  /**
   * Simple method for displaying generic server error toast
   */
  private showAuthorizationError() {
    this.toast.warn(
      `You have no access to the requested resource. `,
      { title: 'No access!', duration: 0 }
    );
  }

  /**
   * Simple method for displaying generic server error toast
   */
  private showPayloadError() {
    this.toast.warn(
      `Check form data or contact us. `,
      { title: 'Invalid request', duration: 0 }
    );
  }

}
