import { Injectable } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd';
import { logger } from '@app/_core/logger.service';

interface ToastOptions {
  title: string; // Recommended to set for web
  duration: number;
  onlyMobile: boolean;
  onlyWeb: boolean;
}

enum ToastTypes {
  plain = 'plain',
  info = 'info',
  success = 'success',
  warn = 'warn',
  error = 'error'
}

/**
 * Centralized toast distribution system a.k.a. `toaster`.
 * Allows us to easily change 3rd party service or create our own templates.
 * Ng Zorro allows custom templates it if you feel default are not good enough.
 * More info: https://ng.ant.design/components/notification/en
 */
@Injectable({
  providedIn: 'root',
})
export class ToasterService {
  private readonly debug: boolean;

  // This may require creating custom templates for toasts
  constructor(
    private notification: NzNotificationService
  ) {
    // this.debug = true; // Uncomment to view sample toast on app start

    // EXAMPLES
    if (this.debug) {
      this.error('This is an error test message. (frozen)', {
        title: 'Error'
      });
      this.info('This is an info test message. (3s)', {
        title: 'Information',
        duration: 3000
      });
      this.plain('This is a plain test message. (5s)', {
        title: 'Plain',
        duration: 5000
      });
      this.success('This is a success test message. (10s)', {
        title: 'Success',
        duration: 10000
      });
      this.warn('This is a warn test message. (15s)', {
        title: 'Warning',
        duration: 15000
      });
      this.plain('This should NOT be displayed!', {
        title: 'MOBILE ONLY',
        onlyMobile: true,
      });
    }
  }

  /**
   * No special styles. Just plain white toast with text and no icon.
   */
  plain(message: string, options: Partial<ToastOptions> = {}) {
    options['title'] = options.title || 'Notification';
    this.showToast(ToastTypes.plain, message, options);
  }

  /**
   * Blue borders and background. Info icon.
   */
  info(message: string, options: Partial<ToastOptions> = {}) {
    options['title'] = options.title || 'Information';
    this.showToast(ToastTypes.info, message, options);
  }

  /**
   * Green borders and background. Check icon.
   */
  success(message: string, options: Partial<ToastOptions> = {}) {
    options['title'] = options.title || 'Success!';
    this.showToast(ToastTypes.success, message, options);
  }

  /**
   * Orange borders and background. Exclamation point icon.
   */
  warn(message: string, options: Partial<ToastOptions> = {}) {
    options['title'] = options.title || 'Warning!';
    this.showToast(ToastTypes.warn, message, options);
  }

  /**
   * Red borders and background. 'X' icon.
   */
  error(message: string, options: Partial<ToastOptions> = {}) {
    options['title'] = options.title || 'Error!';
    this.showToast(ToastTypes.error, message, options);
  }

  /**
   * Centralized method to show toasts.
   */
  private showToast(type: ToastTypes, message: string, options: Partial<ToastOptions>) {
    if (options.onlyMobile) return;
    logger.trace('Displaying toast', type, message, options);

    // Convert mobile duration to number
    const duration = options.duration || 6000;

    // Show toast with styling
    this.notification.blank(options.title || '', message, {
      nzDuration: duration,
      nzClass: `toast-${type}`,
    });

  }
}
