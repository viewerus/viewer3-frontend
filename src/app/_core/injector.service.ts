import { Injectable, Injector } from '@angular/core';

export let injector: Injector;

/**
 * Injector to be used anywhere in the app.
 * Allowing us to clear constructor of dependencies (e.g. for inheritance).
 */
@Injectable({
  providedIn: 'root',
})
export class InjectorService {
  constructor(private inj: Injector) {
    injector = this.inj;
  }
}
