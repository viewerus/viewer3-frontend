import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

// Logger instance without dependency injection
export let logger: LoggerService;

/**
 * Custom logger to allow for easier log control.
 */
@Injectable({
  providedIn: 'root',
})
export class LoggerService {
  public level: number; // Max log levels to display
  public debug: boolean; // If true show all logs on development

  // Possible levels
  public levels = {
    OFF: 0,
    CRITICAL: 50,
    ERROR: 100,
    WARN: 200,
    INFO: 300,
    LOG: 400,
    TRACE: 500,
  };

  constructor() {
    this.debug = true; // Uncomment for trace logs (development only)
    this.level = this.getDefaultLevel();
    logger = this;
  }

  /**
   * Empty function for void binding.
   */
  public noop = () => {};

  /**
   * Only visible in development (ONLY is debug property is true)
   */
  public get trace() {
    if (this.level >= this.levels.TRACE) {
      return console.log.bind(console);
    }
    return this.noop;
  }

  /**
   * Only visible in development (always)
   */
  public get log() {
    if (this.level >= this.levels.LOG) {
      return console.log.bind(console);
    }
    return this.noop;
  }

  /**
   * Visible on production, operational information.
   */
  public get info() {
    if (this.level >= this.levels.INFO) {
      return console.info.bind(console);
    }
    return this.noop;
  }

  /**
   * Visible on production, possible threats, unexpected behaviour.
   */
  public get warn() {
    if (this.level >= this.levels.WARN) {
      return console.warn.bind(console);
    }
    return this.noop;
  }

  /**
   * Always visible, something went wrong.
   */
  public get error() {
    if (this.level >= this.levels.ERROR) {
      return console.error.bind(console);
    }
    return this.noop;
  }

  /**
   * Errors that 'Should not happen'
   */
  public get critical() {
    if (this.level >= this.levels.CRITICAL) {
      return console.error.bind(console);
    }
    return this.noop;
  }


  /**
   * Can be used to determine default log level
   * for a given environment.
   */
  getDefaultLevel() {
    if (!environment.production) {
      // Show log and trace (debug) on development
      return this.debug ? this.levels.TRACE : this.levels.LOG;
    } else {
      // Show info, warn, error, critical on production
      // TODO set it right after testing!
      return this.levels.TRACE;
    }
  }

}
