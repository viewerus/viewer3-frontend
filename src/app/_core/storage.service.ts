import { Injectable } from '@angular/core';
import { Channels, MessengerService } from '@app/_core/messenger.service';
import { logger } from '@app/_core/logger.service';
import { Utils } from '@app/_shared/utils';
import * as _ from 'lodash';

/**
 * All local storage interactions should be done via this service.
 * It's meant to provide safer way to use web-specific storage in
 * mobile app. It also allows event broadcasting and cross tab communication.
 */
@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private readonly lsEnabled: boolean; // Check once then read
  private storage: any; // Object for simulating storage on mobile

  constructor(private messenger: MessengerService) {
    this.lsEnabled = Utils.isLocalStorageAvailable;
    this.storage = {};

    if (this.lsEnabled) {
      this.observeLocalStorage();
    }
  }

  /**
   * Adds or updates item in app's storage.
   */
  public setItem(key: string, value: any) {
    // Serialize objects
    if (_.isObject(value)) {
      value = JSON.stringify(value);
    }

    // Decide which storage to use
    if (this.lsEnabled) {
      localStorage.setItem(key, value);
    } else {
      this.sendStorageEvent({
        key: key,
        newValue: value,
        oldValue: this.storage[key],
      });
      this.storage[key] = value;
    }
  }

  /**
   * Retrieves one by key from storage and returns it.
   */
  public getItem(key: string): string {
    if (this.lsEnabled) { return localStorage.getItem(key); }
    if (!this.lsEnabled) return this.storage[key];
  }

  /**
   * Removes single item by key from storage.
   */
  public removeItem(key: string) {
    if (this.lsEnabled) {
      localStorage.removeItem(key);
    } else {
      this.sendStorageEvent({
        key: key,
        newValue: null,
        oldValue: this.storage[key],
      });
      delete this.storage[key];
    }
  }

  /**
   * Removes all items from storage.
   */
  public clear() {
    if (this.lsEnabled) localStorage.clear();
    if (!this.lsEnabled) {
      this.storage = {};
      this.sendStorageEvent('clear');
    }
  }

  /**
   * Sends message across web tabs by setting and removing storage item.
   */
  public sendBroadcast(text: string, data?: any) {
    this.setItem('broadcast/' + text, data);
    this.removeItem('broadcast/' + text);
  }

  /**
   * Use messengerService to send information about storage event.
   */
  private sendStorageEvent(data: any) {
    if (data.key === 'testingLocalStorage') return; // Ignore Utils testing
    logger.trace('Sending storage event', data);
    this.messenger.send(Channels.storage, 'Storage event', data);
  }

  /**
   * Subscribe to changes in localStorage and send event messages.
   * Only sends messages that contains broadcast in key
   */
  private observeLocalStorage() {
    window.addEventListener('storage', event => {
      logger.trace('New storage event', event);

      // Extract important data from event
      const data = (
        ({ key, newValue, oldValue }) => ({key, newValue, oldValue })
      )(event);

      // Only send broadcast messages
      if (data.key.includes('broadcast') && data.oldValue) {
        return this.messenger.relayBroadcast(data);
      }

      // Send info about event
      this.sendStorageEvent(data);
    });
  }

}
