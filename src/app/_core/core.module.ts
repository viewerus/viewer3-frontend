import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
/* SERVICES */
import { HttpService } from './http.service';
import { HttpInterceptorService } from './http-interceptor.service';
import { LoggerService } from './logger.service';
import { NavigationService } from './navigation.service';
import { StorageService } from './storage.service';
import { ToasterService } from './toaster.service';
import { InjectorService } from './injector.service';

@NgModule({
  declarations: [],
  providers: [
    HttpService,
    HttpInterceptorService,
    LoggerService,
    NavigationService,
    StorageService,
    ToasterService,
    InjectorService,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
  ]
})
export class CoreModule { }
