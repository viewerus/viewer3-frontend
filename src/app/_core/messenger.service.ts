import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

export interface Message {
  text: string;
  data: any;
}

export enum Channels {
  common = 'common',
  auth = 'auth',
  storage = 'storage',
  layout = 'layout',
  broadcast = 'broadcast',
}

/**
 * Service providing basic communication within the app.
 * Based on RxJS observables. Subscribe to channels to receive
 * messages or send message to all subscribers via send method.
 */
@Injectable({
  providedIn: 'root',
})
export class MessengerService {

  public common: // Basic channel where there is nothing else
  Observable<Message> = new Subject();

  public auth: // Channel for authentication stuff (user logged in ect.)
  Observable<Message> = new BehaviorSubject({
    text: 'User is logged out',
    data: { status: 401 }
  });

  public storage: // Local storage events are send here
  Observable<Message> = new Subject();

  public layout: // Used to show drawer, change page title etc.
  Observable<Message> = new Subject();

  public broadcast: // Used to communicate between tabs in the browser
  Observable<Message> = new Subject();

  constructor() {}

  /**
   * Method for sending message and data via given channel.
   * All subscribed components will get the message.
   */
  public send(channel: string, text: string, data?: any) {
    this[channel].next({ text, data });
  }

  /**
   * Received message from other tab.
   */
  public relayBroadcast(data: any) {
    const text = data.key.split('/')[1];
    this.send(Channels.broadcast, text, data.oldValue);
  }

}
