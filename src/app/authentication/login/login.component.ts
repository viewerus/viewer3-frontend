import { Component } from '@angular/core';
import { FormComponent } from '../../_shared/components/form/form.component';
import { Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent extends FormComponent {

  constructor(
    private auth: AuthenticationService,
  ) {
    super();
    this.defaultBtnText = 'Access account';
    this.loadingBtnText = 'Checking...';
    this.submitText = this.defaultBtnText;
  }

  buildForm() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email, Validators.pattern('.+@.+..+')]],
      password: [null, [Validators.required, Validators.minLength(8)]],
    });
  }

  submitForm(): void {
    super.submitForm(); // Make general things first

    // Begin user authentication
    this.auth.login(this.form.value)
      .pipe(
        finalize(() => {
          this.resetForm();
        })
      ).subscribe();
  }

}
