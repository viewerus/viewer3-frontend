import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ActivationComponent } from './activation/activation.component';
import { AccountRecoveryComponent } from './account-recovery/account-recovery.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LoginComponent,
    RegistrationComponent,
    ActivationComponent,
    AccountRecoveryComponent,
    PasswordResetComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
  ]
})
export class AuthenticationModule { }
