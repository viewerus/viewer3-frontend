import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Base64 } from 'js-base64';

import { HttpService } from '@app/_core/http.service';
import { ToasterService } from '@app/_core/toaster.service';
import { StorageService } from '@app/_core/storage.service';
import { MessengerService } from '@app/_core/messenger.service';
import { logger } from '@app/_core/logger.service';

import { finalize, share, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Utils } from '@app/_shared/utils';
import { AccessToken } from '@app/_models/access-token.model';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private _user: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpService,
    private toast: ToasterService,
    private storage: StorageService,
    private messenger: MessengerService,
  ) {

    this.messenger.broadcast.subscribe(msg => {
        if (msg.text === 'logged-out') {
          this.logout(false, true);
        }
      });

  }

  /**
   * Method for accessing authentication service on backend.
   * Sends request with username (email) and password.
   * After response make request for user details and save them in user.model.
   * It also redirects to back url.
   * @param credentials form object containing username and password
   */
  public login(credentials): Observable<any> {
    logger.trace('Logging user in...');

    // Get session for credentials
    return this.http.post('/auth/login', credentials)
      .pipe(
        tap(
          data => {
            logger.info('Login successful', data);
            const token = data.token;

            const payload: AccessToken = Utils.decodeJwt(token);
            if (!payload) throw new Error('Invalid token payload');

            // Initialize session
            this.messenger.send('auth', 'Got token', { payload });

            const user = {
              access_token: token,
              expires_at: new Date(payload.exp * 1000).toISOString(),
              email: payload.email,
            };

            if (payload['admin']) user['admin'] = payload['admin'];
            this.storage.setItem('user', user);

            this.toast.success('You are now logged in', {
              title: 'Hello!',
              duration: 3000 });

            this.loginRedirect();
          },
          error => {
            logger.trace('Login error', error);

            // User not activated
            if (error.status === 401) {
              logger.warn('User is not activated');
              this.toast.warn('Your account has not been activated yet.'
                + 'Check spam folder in your email client for activation link.',
                { title: 'Not activated', duration: 0 });
            }

            // Invalid credentials
            if (error.status === 423) {
              logger.warn('Account blocked.');
              this.toast.error('Account blocked by administrator.',
                { title: 'BLOCKED', duration: 6000 });
            }

            // Invalid credentials
            if (error.status === 400) {
              logger.warn('Invalid username or password.');
              this.toast.error('Invalid username or password.',
                { title: 'Invalid credentials', duration: 6000 });
            }

            // We have unexpected error
            logger.trace('Server error!');
          }
        ),
        share()
      );

  }

  /**
   * Method for adding new user to database.
   * After successful registration redirects to success page.
   * @returns Promise
   */
  public register(user): Observable<any> {
    logger.trace('Registering user...');

    return this.http.post('/auth/register', user)
      .pipe(
        tap(
          data => {
            logger.trace('Registration successful.', data);
          },
          error => {
            logger.trace('Error registering...', error);

            // User not activated
            if (error.status === 409) {
              logger.trace('Account already exists.');
              this.toast.error('Account with this email address already exists.',
                {title: 'Account exists', duration: 15000});
            }

          }
        ),
        share()
      );

  }

  /**
   * Send request for session destruction,
   * Removes sensitive data from application.
   * silent {boolean} is true don't show toast
   */
  public logout(silent?: boolean, skipBroadcast?: boolean) {
    logger.trace('Logging out user...');
    this.storage.removeItem('user');
    this.router.navigate(['/login']);

    // Inform other components about it
    this.messenger.send('auth', 'User logged out', { status: 401 });
    if (!skipBroadcast) {
      // Inform other tabs about it
      this.storage.sendBroadcast('logged-out', { status: 401 });
    }

    // Inform user about success
    if (!silent) {
      this.toast.success('You have been successfully logged out.', {
        title: 'Good bye!',
        duration: 3000
      });
    }

  }

  /**
   * Activate user account.
   */
  public activate(token: string): Observable<any> {
    logger.trace('Activating user with token', token);
    return this.http.post('/auth/activate', {token});
  }

  /**
   * Send password reset link to the user
   */
  public recoverAccount(email: {email: string}): Observable<any> {
    logger.trace('Sending recovery link to', email);
    return this.http.post('/auth/recover-account', email);
  }

  /**
   * Send new password to the server
   */
  public resetPassword(token: string, password: string): Observable<any> {
    return this.http.post('/auth/reset-password', {token, password});
  }

  public loginRedirect() {
    // Get return url from route
    const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    // Redirect user
    logger.trace('Redirecting user to', returnUrl);
    this.router.navigate([returnUrl]).then();
  }

  public async resendActivationEmail(email: string) {
    return await this.http.post('/auth/resend-activation-email', {email})
      .subscribe();
  }

  /**
   * Simple wrapper for checking whether we have user object in storage.
   */
  get isLoggedIn(): boolean {
    logger.trace('Checking if user is logged in...');
    return (!!this.user && !!this.user.email);
  }

  /**
   * Get cached user
   */
  get user(): any {
    // Parse user from local storage (if exists)
    let user: any = this.storage.getItem('user');
    user = user && JSON.parse(user);

    logger.log('Returning user', user);
    return user;
  }

  /**
   * Set new value for user
   */
  set user(user: any) {
    user = typeof user === 'string' ? JSON.parse(user) : user;

    logger.trace('Setting user', user);
    this._user = user;
    this.messenger.send('auth', 'User changed', user);
  }

}
