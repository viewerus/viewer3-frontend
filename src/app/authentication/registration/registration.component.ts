import { Component, OnInit } from '@angular/core';
import { FormComponent } from '@app/_shared/components/form/form.component';
import { Validators } from '@angular/forms';
import { AuthenticationService } from '@app/authentication/authentication.service';
import { ToasterService } from '@app/_core/toaster.service';
import { CustomValidators } from '@app/_shared/custom-validators';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
})
export class RegistrationComponent extends FormComponent implements OnInit {
  public allowResend: boolean;

  constructor(
    private auth: AuthenticationService,
    private toast: ToasterService
  ) {
    super();
    this.defaultBtnText = 'Create account';
    this.loadingBtnText = 'Saving...';
    this.submitText = this.defaultBtnText;
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email, Validators.pattern('.+@.+..+')]],
      password: [null, [Validators.required, Validators.minLength(8)]],
      repeatedPassword: [null, [Validators.required, Validators.minLength(8), CustomValidators.passwordsMatch]],
    });
  }

  submitForm() {
    super.submitForm();

    // Disable resend button
    this.allowResend = false;

    // Register user
    this.auth.register(this.form.value)
      .pipe(
        tap(
          data => {
            this.submitted = true;
            this.startResendTimer();
            this.toast.success(`Check your email for activation link.`, {
              title: 'Account created',
              duration: 10000 });
          },
          error => {
            this.resetForm();
          }
        )).subscribe();
  }

  async resendActivation() {
    const email = this.form.get('email').value;
    if (!email) return;
    this.auth.resendActivationEmail(email).then(
      data => {
        this.toast.success(`Check your email (and SPAM folder).`, {
          title: 'Success',
          duration: 10000 });
        this.allowResend = false; // Disable button
        this.startResendTimer();
      },
      error => {
        this.toast.error(`Could not send an email.`, { title: 'Error' });
      }
    );
  }

  /**
   * Re-enable resend button after specified period of time
   * @param duration Time before button is re-enabled in seconds (10 min)
   */
  startResendTimer(duration: number = 10 * 60 * 1000) {
    setTimeout(() => {
      this.allowResend = true;
    }, duration);
  }

}
