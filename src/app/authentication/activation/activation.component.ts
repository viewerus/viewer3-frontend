import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@app/authentication/authentication.service';
import { finalize, tap } from 'rxjs/operators';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
})
export class ActivationComponent {
  public clientMessage: string;
  public ready: boolean;

  constructor(
    private route: ActivatedRoute,
    private auth: AuthenticationService,
  ) {
    this.clientMessage = 'Validating activation request...';
    const token = this.route.snapshot.params['token'];
    setTimeout(() => {
      this.activate(token);
    }, 1000);

  }

  activate(token: string) {
    this.auth.activate(token).pipe(
      tap(
        data => {
          this.clientMessage =
            ' Account active. ' +
            'Log in and start using Viewer.';
          this.ready = true;
        },
        error => {
          this.clientMessage =
            'Error activating account. ' +
            'Most likely token has expired. If so create account again. ' +
            'If you copied link from email make sure it\'s ok. ';
        }
      )
    ).subscribe();
  }

}
