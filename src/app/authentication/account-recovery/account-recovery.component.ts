import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '@app/authentication/authentication.service';
import { FormComponent } from '@app/_shared/components/form/form.component';
import { Router } from '@angular/router';
import { ToasterService } from '@app/_core/toaster.service';

@Component({
  selector: 'app-account-recovery',
  templateUrl: './account-recovery.component.html',
})
export class AccountRecoveryComponent extends FormComponent implements OnInit {

  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private toast: ToasterService,
  ) {
    super();
    this.defaultBtnText = 'Send instructions';
    this.loadingBtnText = 'Sending...';
    this.submitText = this.defaultBtnText;
  }

  buildForm() {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email, Validators.pattern('.+@.+..+')]],
    });
  }

  submitForm(): void {
    super.submitForm(); // Make general things first

    // Send recovery email
    this.auth.recoverAccount(this.form.value)
      .pipe(
        finalize(() => {
          this.resetForm();
          this.toast.success(`Check your email and SPAM folder.`, {
            title: 'EMAIL SENT',
            duration: 10000 });

          this.router.navigate(['\login']);
        })
      ).subscribe();
  }
}
