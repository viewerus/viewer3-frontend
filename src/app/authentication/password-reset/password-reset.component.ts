import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators } from '@angular/forms';
import { AuthenticationService } from '@app/authentication/authentication.service';
import { ToasterService } from '@app/_core/toaster.service';
import { CustomValidators } from '@app/_shared/custom-validators';
import { FormComponent } from '@app/_shared/components/form/form.component';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
})
export class PasswordResetComponent extends FormComponent implements OnInit {

  constructor(
    private auth: AuthenticationService,
    private toast: ToasterService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    super();
    this.defaultBtnText = 'Change password';
    this.loadingBtnText = 'Saving...';
    this.submitText = this.defaultBtnText;
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this.formBuilder.group({
      password: [null, [Validators.required, Validators.minLength(8)]],
      repeatedPassword: [null, [Validators.required, Validators.minLength(8), CustomValidators.passwordsMatch]],
    });
  }

  submitForm() {
    super.submitForm();

    const token = this.route.snapshot.params['token'];

    // Register user
    this.auth.resetPassword(token, this.form.get('password').value)
      .pipe(
        tap(
          data => {
            this.toast.success(`You can log in now.`, {
              title: 'Changes saved',
              duration: 10000 });
            this.router.navigate(['/login']);
          },
          error => {
            this.resetForm();
          }
        )).subscribe();
  }

}
