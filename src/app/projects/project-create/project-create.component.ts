import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take, tap } from 'rxjs/operators';
import { FormComponent } from '@app/_shared/components/form/form.component';
import { ToasterService } from '@app/_core/toaster.service';
import { ProjectService } from '@app/projects/project.service';
import { UploadFile } from 'ng-zorro-antd';

type Modes = 'menu' | 'microscope' | 'manual' |
             'coordinates' | 'model';

interface Mode {
  name: Modes;
  description: string;
  extensions?: string[];
  sizeLimit?: number;
}

interface FileDescriptor {
  valid: boolean;
  message: string;
  name: string;
}

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
})
export class ProjectCreateComponent extends FormComponent {
  public mode: Mode;
  public modes: {[x: string]: Mode};
  public file: Partial<FileDescriptor>;
  public handleFile: any;

  constructor(
    private service: ProjectService,
    private router: Router,
    private toast: ToasterService,
    private route: ActivatedRoute,
  ) {
    super();
    this.defaultBtnText = 'Save changes';
    this.loadingBtnText = 'Saving...';
    this.submitText = this.defaultBtnText;
    this.file = {};

    this.modes = {
      menu: {
        name: 'menu',
        description: 'Choose a method used to add model to the project.',
      },
      microscope: {
        name: 'microscope',
        description: 'Upload image and let the server generate 3D model out of it.',
        extensions: ['tiff'],
        sizeLimit: 100 * 1000 * 1000, // 100 mb
      },
      manual: {
        name: 'manual',
        description: 'Fill form below to generate a new model.',
      },
      coordinates: {
        name: 'coordinates',
        description: 'Upload csv file with coordinates to generate the model.',
        extensions: ['csv'],
        sizeLimit: 100 * 1000 * 1000, // 100 mb
      },
      model: {
        name: 'model',
        description: 'Upload complete JSON model.',
        extensions: ['json'],
        sizeLimit: 1000 * 1000 * 1000, // 1000 mb
      },
    };

    this.mode = this.modes['menu'];

    this.handleFile = (file: UploadFile) => {
      this.addFile(file);
      return false;
    };
  }

  buildForm() {
    this.form = this.formBuilder.group({
      name: [null, [Validators.required, Validators.maxLength(60)]],
      description: [null, []],
      fileType: [null, [Validators.required]],
      file: [null, [Validators.required]],
    });
  }

  submitForm(): void {
    super.submitForm();

    const body = new FormData();
    body.append('name', this.form.get('name').value);
    body.append('fileType', this.form.get('fileType').value);
    body.append('file', this.form.get('file').value);

    // Optional fields (necessary to send null instead of 'null' in FormData)
    if (this.form.get('description').value) {
      body.append('description', this.form.get('description').value);
    }

    this.service.create(body, this.mode.name)
      .pipe(
        take(1),
        tap(
          data => {
            this.toast.success('Changes saved.', {title: 'Saved'});
            this.router.navigate(['/']);
          },
          error => {
            this.toast.error('Error saving changes.', {title: 'Error'});
            this.resetForm();
          }
        )
      ).subscribe();
  }

  selectMode(mode: Modes) {
    this.mode = this.modes[mode];
    this.file = {};
    this.form.get('fileType').setValue(null);
    this.form.get('file').setValue(null);
  }

  addFile(file: UploadFile) {
    this.validateFile(file, this.mode.extensions, this.mode.sizeLimit);

    if (this.file.valid) {
      this.form.get('fileType').setValue(this.mode.name);
      this.form.get('file').setValue(file);
      this.triggerFormUpdate();
    }
  }

  validateFile(file: UploadFile, extensions?: string[], maxSize?: number) {

    // Check extension
    const extension = file.name.split('.').pop();
    if (extensions && !extensions.includes(extension)) {
      this.file.valid = false;
      this.file.message = `Invalid extension. Allowed: ${ extensions.join(', ') } `;
      return;
    }

    // Check size
    if (maxSize !== undefined && file.size > maxSize) {
      this.file.valid = false;
      this.file.message = `Exceeded max file size of ${maxSize / 1000} kb`;
      return;
    }

    // Sanitize name
    this.file.name = file.name
        .replace(extension, '')
        .replace(/[^a-zA-Z0-9-]/gi, '')
        .replace(/[\x00-\x1f\x80-\x9f]/g, '')
        .toLowerCase()
        .substring(0, (60 - extension.length - 1))
        + '.' + extension;

    this.file.valid = true;
    this.file.message = `Selected file: ${this.file.name}`;
  }

  showHint() {
    // TODO show modal with help info about formats
    return null;
  }
}
