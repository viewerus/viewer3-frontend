import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { ProjectService } from '@app/projects/project.service';
import { ProjectListComponent } from '@app/projects/project-list/project-list.component';
import { ProjectDetailsComponent } from '@app/projects/project-details/project-details.component';
import { ProjectCreateComponent } from '@app/projects/project-create/project-create.component';
import { ProjectDetailsResolver } from '@app/projects/_resolvers/project-details.resolver';
import { ProjectListResolver } from '@app/projects/_resolvers/project-list.resolver';
import { ElasticInputModule } from 'angular2-elastic-input';
import { ViewerModule } from '@app/viewer/viewer.module';

@NgModule({
  declarations: [
    ProjectDetailsComponent,
    ProjectListComponent,
    ProjectCreateComponent,
  ],
  providers: [
    ProjectService,
    ProjectDetailsResolver,
    ProjectListResolver,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgZorroAntdModule,
    ReactiveFormsModule,
    ElasticInputModule,
    /**/
    ViewerModule,
  ]
})
export class ProjectModule {}
