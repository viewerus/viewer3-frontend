import { HttpService } from '@app/_core/http.service';
import { Observable } from 'rxjs';
import { logger } from '@app/_core/logger.service';
import { share, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()
export class ProjectService {

  constructor(
    private http: HttpService,
  ) {}

  getAll(): Observable<any[]> {
    logger.log('Getting analyses...');

    return this.http.get('/projects').pipe(
      tap(
        response => {
          logger.log('Got projects', response);
        },
        error => {
          logger.log('Error getting projects', error);
        }
      ),
      share()
    );
  }

  getDetails(id: string): Observable<any> {
    return this.http.get(`/projects/${id}`);
  }

  update(id: string, data: any): Observable<any> {
    return this.http.patch(`/projects/${id}`, data);
  }

  delete(id: string): Observable<any> {
    logger.log('Removing project...', id);

    return this.http.delete(`/projects/${id}`).pipe(
      tap(
        response => {
          logger.log('Project removed', response);
        },
        error => {
          logger.log('Error removing project', error);
        }
      ),
      share()
    );
  }

  create(data: any, mode): Observable<any> {
    logger.log('Creating project...');

    const types = {
      microscope: 'microscope',
      manual: 'data',
      coordinates: 'spreadsheet',
      model: 'model',
    };

    return this.http.post(`/projects/${types[mode]}`, data).pipe(
      tap(
        response => {
          logger.log('Created project', response);
        },
        error => {
          logger.log('Error creating project', error);
        }
      ),
      share()
    );
  }

  getModel(id) {

    return this.http.get(`/files/${id}`).pipe(
      tap(
        response => {
          logger.log('Created project', response);
        },
        error => {
          logger.log('Error creating project', error);
        }
      ),
      share()
    );

  }

}
