import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ProjectService } from '@app/projects/project.service';

@Injectable()
export class ProjectDetailsResolver implements Resolve<Observable<any>> {

  constructor(
    private service: ProjectService,
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.service.getDetails(route.params['id']);
  }
}
