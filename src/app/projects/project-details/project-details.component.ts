import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '@app/projects/project.service';
import { take, tap } from 'rxjs/operators';
import { ToasterService } from '@app/_core/toaster.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
})
export class ProjectDetailsComponent implements OnInit {
  public item: any;
  private itemId: string;
  public editable: {[x: string]: boolean};
  public coordinates;
  public showCompatibility: boolean;

  constructor(
    private toast: ToasterService,
    private service: ProjectService,
    private route: ActivatedRoute,
  ) {
    this.item = this.route.snapshot.data.resolverData;
    this.itemId = this.route.snapshot.params['id'];
    this.editable = {
      name: false,
      description: false,
    };

    this.service.getModel(this.item.files[0].id).pipe(
      take(1),
      tap(data => {
        this.coordinates = data;
      }),
    ).subscribe();
  }

  ngOnInit() {}

  toggleEdit(fieldName: string, element?: HTMLInputElement) {
    this.editable[fieldName] = !this.editable[fieldName];
    element.focus();
  }

  saveEdit(fieldName: string, element: HTMLInputElement) {
    this.editable[fieldName] = false;
    const body = {[fieldName]: element.value};

    this.service.update(this.itemId, body)
      .pipe(
        take(1),
        tap(
          data => {
            this.toast.success('Changed has been saved.', {title: 'Saved'});
            this.item[fieldName] = data[fieldName];
          },
          error => {
            this.toast.error('Changes could not be saved.', {title: 'Error!'});
            element.value = this.item[fieldName];
          }
        )
      ).subscribe();
  }

  cancelEdit(fieldName: string, element: HTMLInputElement) {
    this.editable[fieldName] = false;
    element.value = this.item[fieldName];
  }

}
