import { TableComponent } from '@app/_shared/components/table/table.component';
import { ProjectService } from '@app/projects/project.service';
import { logger } from '@app/_core/logger.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToasterService } from '@app/_core/toaster.service';
import { delay, take, tap } from 'rxjs/operators';
import { AuthenticationService } from '@app/authentication/authentication.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
})
export class ProjectListComponent extends TableComponent implements OnInit {
  public lastUpdate: Date;
  public disableCreation;

  constructor(
    private auth: AuthenticationService,
    private route: ActivatedRoute,
    private toast: ToasterService,
    private projectService: ProjectService,
  ) {
    super();
  }

  ngOnInit() {
    this.initProperties();
    this.initData();
    this.limit = 1000;
    this.sort('createdAt', 'descend'); // Latest first
    this.ready = true;
    this.lastUpdate = new Date();

    const email = this.auth.user && this.auth.user.email || '';
    this.disableCreation = (email === 'demo@demo.pl');
  }

  /**
   * Assign default or initial values to properties.
   */
  initProperties() {
    super.initProperties();
    this.sortColumn = 'name';

    this.sortMap = {
      name: null,
      description: null,
    };
  }

  /**
   * Get data fetched by resolver
   */
  initData() {
    const items = this.route.snapshot.data.resolverData;
    logger.trace('Received data from resolver', this.data);
    this.data = items;
    this.processData();
    this.itemsCount = (this.data && this.data.length) || 0;
  }

  /**
   * Shortens a string to a given length.
   * If text was larger than limit adds '...' at the end.
   */
  truncate(text: string, length: number) {
    if (!text) return;
    const suffix = (text.length >= length) ? '...' : '';
    return text.substr(0, length) + suffix;
  }

  async fetchData(): Promise<any> {
    this.lastUpdate = new Date();
    this.loading = true;
    this.projectService.getAll()
      .pipe(
        delay(500),
        take(1),
        tap(data => {
          this.data = data;
          this.processData();
          this.fetchPage();
        })
      ).subscribe();
  }

  processData() {
    for (const item of this.data) {
      item.description = this.truncate(item.description, 255);
    }
  }

  remove(id: string) {
    this.loading = true;
    this.projectService.delete(id)
      .pipe(
        tap(
          success => {
            this.toast.success('Project deleted', {title: 'Success'});
            this.fetchData();
          },
          error => {
            console.error('Error removing project', error);
            this.toast.error('Project could not be deleted', {title: 'Error'});
          }
        )
      )
      .subscribe();
    this.fetchData();
  }


}
