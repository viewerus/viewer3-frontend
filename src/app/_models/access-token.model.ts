import { BaseToken } from '@app/_models/base-token.model';

export interface AccessToken extends BaseToken {
  email: string;
  roles: string[];
}
