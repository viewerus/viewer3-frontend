export interface BaseToken {
  type: string; // Type of a token
  iat: number; // Date as seconds - generation date
  exp: number; // Date as seconds - expiration date
}
