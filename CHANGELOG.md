# Changelog
All notable changes to this project will be documented in this file.

## [1.0.0] - Unreleased (current master)
### Added
- Added optional performance widget to model view
- Added color picker panel
- Implemented tutorial tour for viewer component
- Added visibility action in top menu for viewer component

### Changed
- Disabled OPEN button for projects not ready to be displayed (e.g. parsing not finished)
- Accessibility audit and fixes
- Improved camera and materials performance (technical)

### Fixes
- Fixed invalid hover behaviour for model groups
- Fixed invalid redirection on 403
- Fixed building issues (AoT)
- Fixed numerous issues with coloring and selection in model view

## [0.0.1] - 2018-22-22
### Added
- Created login and registration pages
- Created password recovery pages
- Created activation pages and email templates
- Created error pages
- Created project list view with cards
- Created project creation view with file upload and validation
- Created project details view with editing capabilities
- Implemented viewer component for model display
- Added basic guards
- Implemented "builder" docker container
- Added contact and bug report feature
- Added tree view to viewer component
- Implemented top menu in viewer component
- Implemented selection, locking and interaction properties for groups in viewer component
- Implemented model exporting and screenshot saving
- Added deletion confirmation box for project removal
- Implemented sorting, search and refresh in list view
